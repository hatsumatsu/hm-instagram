<?php

/*
Plugin Name: HM Instagram
Version: 0.8.2
Description: Automatically import Instagram posts into Wordpress
Plugin URI: http://martinwecke.de/
Author: Martin Wecke
Author URI: http://martinwecke.de/
Bitbucket Plugin URI: https://bitbucket.org/hatsumatsu/hm-instagram/
Bitbucket Branch: master
*/
class HMInstagram {
    public function __construct() {
        /**
         * Core
         */
        require_once( dirname( __FILE__ ) . '/includes/HMInstagram_Core.php' );
        new HMInstagram_Core();

        // register activation / deactivation hook
        register_activation_hook( __FILE__, array( 'HMInstagram_Core', 'activatePlugin' ) );


        /**
         * Sync
         */
        require_once( dirname( __FILE__ ) . '/includes/HMInstagram_Sync.php' );
        new HMInstagram_Sync();


        /**
         * Auth
         */
        require_once( dirname( __FILE__ ) . '/includes/HMInstagram_Auth.php' );
        new HMInstagram_Auth();


        /**
         * Admin
         */
        require_once( dirname( __FILE__ ) . '/includes/HMInstagram_Admin.php' );
        new HMInstagram_Admin();


        /**
         * Posts
         */
        require_once( dirname( __FILE__ ) . '/includes/HMInstagram_Posts.php' );
        new HMInstagram_Posts();

        // register activation / deactivation hook
        register_activation_hook( __FILE__, array( 'HMInstagram_Posts', 'activatePlugin' ) );


        /**
         * Schedule
         */
        require_once( dirname( __FILE__ ) . '/includes/HMInstagram_Schedule.php' );
        new HMInstagram_Schedule();

        // register activation / deactivation hook
        register_activation_hook( __FILE__, array( 'HMInstagram_Schedule', 'activatePlugin' ) );
        register_deactivation_hook( __FILE__, array( 'HMInstagram_Schedule', 'deactivatePlugin' ) );


        /**
         * Settings
         */
        require_once( dirname( __FILE__ ) . '/lib/settings-api/class.settings-api.php' );
        require_once( dirname( __FILE__ ) . '/includes/HMInstagram_Settings.php' );
        new HMInstagram_Settings();


        /**
         * Log
         */
        require_once( dirname( __FILE__ ) . '/includes/HMInstagram_Log.php' );
        new HMInstagram_Log();


        /**
         * Stats
         */
        require_once( dirname( __FILE__ ) . '/includes/HMInstagram_Stats.php' );
        new HMInstagram_Stats();
    }
}

new HMInstagram();
