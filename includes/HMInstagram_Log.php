<?php

class HMInstagram_Log {
    public function __construct() {
        $this->init();
    }


    /**
     * Init
     */
    public function init() {

    }


    /**
     * Log
     * @param  string $message message
     */
    public static function log( $message ) {
        $settings = get_option( 'instagram--debug' );

        if( !isset( $settings['logging'] ) || $settings['logging'] === 'off' ) {
            return false;
        }

        if( !$message ) {
            return false;
        }

        $message = 'HM Instagram: ' . $message;

        $context = array(
            'plugin_name' => __( 'HM Instagram', 'hm-instagram' )
        );

        if( function_exists( 'SimpleLogger' ) ) {
            SimpleLogger()->info(
                $message,
                $context
            );
        }
    }
}
