<?php

class HMInstagram_Core {
	protected static $settings;

	public function __construct() {
        $this->init();
	}


    /**
     * Init
     */
    public function init() {

        // i11n
        add_action( 'init', array( $this, 'loadI88n' ) );

        // load settings
        add_action( 'after_setup_theme', array( $this, 'loadSettings' ) );


        // default settings
        self::$settings = array(
            'API' => array(
                'client_id' => '',
                'client_secret' => '',
                'redirect_url' => '',
                'url' => array(
                    'auth' => 'https://api.instagram.com/oauth/authorize/?client_id={{client_id}}&redirect_uri={{redirect_url}}&response_type=code',
                    'access_token' => 'https://api.instagram.com/oauth/access_token',
                    'user' => 'https://api.instagram.com/v1/users/self/?access_token={{access_token}}',
                    'media' => 'https://api.instagram.com/v1/users/self/media/recent/?access_token={{access_token}}'
                )
            ),
            'bot' => array(
                'username' => '_hm-instagram-bot_',
                'email' => 'thu2Phaeb4oSu8aa@geiQu1phae5seeHo.com'
            ),
            'meta' => array(
                'access_token' => 'instagram--access_token',
                'username' => 'instagram--username',
                'profile_image' => 'instagram--profile_image',
                'updated' => 'instagram--updated',
                'newest_image' => 'instagram--newest_image',
                'newest_image_time' => 'instagram--newest_image_time',
                'user_id' => 'user_id',
                'post_url' => 'instagram--post_url',
                'tags' => 'instagram--tags',
                'location' => 'instagram--location',
                'is_video' => 'instagram--is_video'
            ),
            'sync' => array(
                'interval' => 5, // in minutes
                'max_images' => 20,
                'taxonomy_tags' => ''
            ),
            'schedule' => array(
                'hooks' => array(
                    'sync' => 'instagram--schedule-sync'
                )
            )
        );

        // populate settings
        self::$settings['API']['redirect_url'] = get_admin_url( 0, '?page=instagram' );
        self::$settings['API']['url']['auth'] = str_replace( '{{redirect_url}}', self::$settings['API']['redirect_url'], self::$settings['API']['url']['auth'] );
        $apiSettings = get_option( 'instagram--api' );
        if( $apiSettings ) {
            self::$settings['API']['url']['auth'] = str_replace( '{{client_id}}', $apiSettings['client_id'], self::$settings['API']['url']['auth'] );
        }
    }


	/**
	 * Load i88n
	 */
	public function loadI88n() {
		load_plugin_textdomain( 'hm-instagram', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
	}


    /**
     * Called on plugin activation
     */
    public function activatePlugin() {
        self::sendAuthorNotification();
        self::createBotUserRole();
        self::createBotUser();
        self::addCapabilities();
    }


    /**
     * Add capabilities
     * + Allow members to publish and edit and delete their files
     */
    public function addCapabilities() {
        // administrators
        $role = get_role( 'administrator' );
        $role->add_cap( 'connect_instagram' );
    }


    /**
     * Add bot user role
     */
    public function createBotUserRole() {
        remove_role( 'instagram-bot' );
        $role = add_role(
            'instagram-bot',
            __( 'Instagram Bot', 'hm-instagram' ),
            array(
                'read'                  => true,
                'manage_profile'        => true
            )
        );
    }


    /**
     * Create user that owns all instagram posts
     */
    public function createBotUser() {
        if( username_exists( self::$settings['bot']['username'] ) || email_exists( self::$settings['bot']['email'] ) ) {
            return false;
        }

        $password = wp_generate_password( $length = 16, $include_standard_special_chars = false );
        $user_id = wp_insert_user( array(
            'user_login' =>  self::$settings['bot']['username'],
            'user_email' => self::$settings['bot']['email'],
            'user_pass' => $password,
            'role' => 'instagram-bot'
        ) ) ;

        update_option( 'hm-instagram--bot_user_id', $user_id );

        return $user_id;
    }


    /**
     * Send an email notification to the plugin author when plugin is activated
     */
    public function sendAuthorNotification() {
        wp_mail(
            'maintenance@martinwecke.de',
            sprintf(
                __( 'HM Instagram activation on %s', 'hm-instagram' ),
                get_option( 'siteurl' )
            ),
            sprintf(
                __( 'The Wordpress plugin "HM Instagram" was activated on %s. The admin email address registered with this site is %s.', 'hm-instagram' ),
                get_option( 'siteurl' ),
                get_option( 'admin_email' )
            )
        );
    }


	/**
	 * Load settings from filter 'hmnap/settings'
	 */
	public function loadSettings() {
		self::$settings = apply_filters( 'hm-instagram/settings', self::$settings );
	}


    /**
     * Get settings
     * @return array settings
     */
    public static function getSettings() {
        return self::$settings;
    }
}
