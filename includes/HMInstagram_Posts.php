<?php

class HMInstagram_Posts {
    public function __construct() {
        $this->init();
    }


    /**
     * Init
     */
    public function init() {
        // register post type
        add_action( 'init', array( $this, 'registerPostType' ) );

        // add meta capabilties
        add_filter( 'map_meta_cap', array( $this, 'addMetaCapabilities' ), 999, 4 );
    }


    /**
     * Called on plugin activation
     */
    public function activatePlugin() {
        self::addCapabilities();
    }


    /**
     * Register post type
     */
    public function registerPostType() {
        $settings = HMInstagram_Core::getSettings();

        register_post_type(
            'instagram',
            array(
                'labels' => array(
                    'name' => __( 'Instagram Posts', 'hm-instagram' ),
                    'singular_name' => __( 'Instagram Post', 'hm-instagram' ),
                    'menu_name' => __( 'Instagram Posts', 'hm-instagram' ),
                    'menu_admin_bar' => __( 'Instagram Posts', 'hm-instagram' ),
                    'all_items' => __( 'All Posts', 'hm-instagram' ),
                    'add_new_item' => __( 'Add new Post', 'hm-instagram' ),
                    'edit_item' => __( 'Edit Post', 'hm-instagram' ),
                    'new_item' => __( 'New Post', 'hm-instagram' ),
                    'view_item' => __( 'View Post', 'hm-instagram' ),
                    'search_items' => __( 'Search Posts', 'hm-instagram' )
                ),
                'capability_type' => 'instagram',
                'capabilities' => array(
                    'publish_posts' => 'publish_instagram_posts',
                    'edit_posts' => 'edit_instagram_posts',
                    'edit_others_posts' => 'edit_others_instagram_posts',
                    'edit_published_posts' => 'edit_published_instagram_posts',
                    'delete_posts' => 'delete_instagram_posts',
                    'delete_others_posts' => 'delete_others_instagram_posts',
                    'read_private_posts' => 'read_private_instagram_posts',
                    'edit_post' => 'edit_instagram_post',
                    'delete_post' => 'delete_instagram_post',
                    'read_post' => 'read_instagram_post'
                ),
                'supports' => array(
                    'title',
                    'editor',
                    'thumbnail',
                    'author'
                ),
                'public' => false,
                'show_ui' => true,
                'menu_position' => 15,
                'menu_icon' => 'dashicons-camera',
                'rewrite' => false,
                'has_archive' => false
            )
        );
    }

    /**
     * Set capabilities
     */
    public function addCapabilities() {
        // bot
        $role = get_role( 'instagram-bot' );

        $role->add_cap( 'edit_instagram_posts' );
        $role->add_cap( 'publish_instagram_posts' );
        $role->add_cap( 'delete_instagram_posts' );
        $role->add_cap( 'edit_published_instagram_posts' );
        $role->add_cap( 'edit_others_instagram_posts' );
        $role->add_cap( 'delete_others_instagram_posts' );
        $role->add_cap( 'read_private_instagram_posts' );

        // admin
        $role = get_role( 'administrator' );

        $role->add_cap( 'edit_instagram_posts' );
        $role->add_cap( 'publish_instagram_posts' );
        $role->add_cap( 'delete_instagram_posts' );
        $role->add_cap( 'edit_published_instagram_posts' );
        $role->add_cap( 'edit_others_instagram_posts' );
        $role->add_cap( 'delete_others_instagram_posts' );
        $role->add_cap( 'read_private_instagram_posts' );
    }


    /**
     * Modify capability check
     * What we want:
     * + Allow admins to edit and delete others posts
     * + Allow bots to edit and delete others posts
     *
     * http://justintadlock.com/archives/2010/07/10/meta-capabilities-for-custom-post-types
     * http://wordpress.stackexchange.com/a/57373/51687
     * http://codex.wordpress.org/Plugin_API/Filter_Reference/user_has_cap
     * @param  [type] $user_caps all the user's capabilities
     * @param  [type] $req_cap   the reqeusted capability
     * @param  [type] $args      arguments
     * @return [type]            modified capability
     */
    public function addMetaCapabilities( $caps, $cap, $user_id, $args ) {
        global $bff_blog_config;

        // If editing, deleting, or reading a blog post, get the post and post type object.
        if ( 'edit_instagram_post' == $cap || 'delete_instagram_post' == $cap || 'read_instagram_post' == $cap ) {
            $post = get_post( $args[0] );
            $post_type = get_post_type_object( $post->post_type );

            // Set an empty array for the caps.
            $caps = array();
        }

        // If editing a blog post, assign the required capability.
        if ( 'edit_instagram_post' == $cap ) {
            if( $user_id == $post->post_author ) {
                if( $post->post_status == 'publish' ) {
                    $caps[] = $post_type->cap->edit_published_posts;
                } elseif( $post->post_status == 'future' ) {
                    $caps[] = $post_type->cap->edit_published_posts;
                } else {
                    $caps[] = $post_type->cap->edit_posts;
                }
            } else {
                $caps[] = $post_type->cap->edit_others_posts;
            }
        }

        // If deleting a blog post, assign the required capability.
        elseif ( 'delete_instagram_post' == $cap ) {
            if ( $user_id == $post->post_author ) {
                $caps[] = $post_type->cap->delete_posts;
            } else {
                $caps[] = $post_type->cap->delete_others_posts;
            }
        }

        // If reading a private blog post, assign the required capability.
        elseif ( 'read_instagram_post' == $cap ) {

            if ( 'private' != $post->post_status ) {
                $caps[] = 'read';
            } elseif ( $user_id == $post->post_author ) {
                $caps[] = 'read';
            } else {
                $caps[] = $post_type->cap->read_private_posts;
            }
        }

        // Return the capabilities required by the user.
        return $caps;
    }


    public static function getPosts( $arguments = array() ) {
        $settings = HMInstagram_Core::getSettings();

        $arguments['post_type'] = 'instagram';
        $arguments['orderby'] = 'post_date';
        $arguments['order'] = 'DESC';

        return get_posts( $arguments );
    }


    /**
     * Delete an Instagram post
     * @param  int $post_id post ID
     */
    public static function deletePost( $post_id ) {
        $settings = HMInstagram_Core::getSettings();

        if( !$post_id ) {
            return false;
        }

        if( $thumbnail_id = get_post_thumbnail_id( $post_id ) ) {
            wp_delete_attachment( $thumbnail_id, true );
        }

        wp_delete_post( $post_id, true );
    }


    /**
     * Delete all Instagram posts
     */
    public static function deletePosts() {
        $settings = HMInstagram_Core::getSettings();

        $posts = get_posts(
            array(
                'post_type'         => 'instagram',
                'posts_per_page'    => -1
            )
        );

        if( !$posts ) {
            return false;
        }

        foreach( $posts as $post ) {
            if( $thumbnail_id = get_post_thumbnail_id( $post->ID ) ) {
                wp_delete_attachment( $thumbnail_id, true );
            }

            wp_delete_post( $post->ID, true );
        }

        HMInstagram_Log::log( sprintf( __( 'Deleted %s Instagram posts.', 'hm-instagram' ), count( $posts ) ) );
    }
}
