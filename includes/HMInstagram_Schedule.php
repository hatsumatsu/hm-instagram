<?php

class HMInstagram_Schedule {
    public function __construct() {
        $this->init();
    }


    /**
     * Init
     */
    public function init() {
        include_once( dirname( __FILE__ ) . '/HMInstagram_Schedule.php' );

        // register custom interval
        add_filter( 'cron_schedules', array( $this, 'registerInterval' ) );
    }


    /**
     * Activation hook
     */
    public static function activatePlugin() {
        self::schedule();
    }


    /**
     * Deactivation hook
     */
    public static function deactivatePlugin() {
        self::unSchedule();
    }


    /**
     * Register custom intervals
     * @param  array $schedules original intervals
     * @return array            modified intervals
     */
    public function registerInterval( $schedules ) {
        $settings = HMInstagram_Core::getSettings();
        $syncSettings = ( get_option( 'instagram--sync' ) ) ? get_option( 'instagram--sync' ) : $settings['sync'];

        $interval = intval( $syncSettings['interval'] ) * 60;

        $schedules[$settings['schedule']['hooks']['sync']] = array(
            'interval'  => $interval,
            'display'   => __( 'Instagram Sync', 'hm-instagram' )
        );

        return $schedules;
    }


    /**
     * Register sync schedule
     */
    public function schedule() {
        $settings = HMInstagram_Core::getSettings();

        if( !wp_next_scheduled( $settings['schedule']['hooks']['sync'] ) ) {
            wp_schedule_event( time(), $settings['schedule']['hooks']['sync'], $settings['schedule']['hooks']['sync'] );
        }
    }


    /**
     * Unregister sync schedule
     */
    public function unSchedule() {
        $settings = HMInstagram_Core::getSettings();

        $timeNext = wp_next_scheduled( $settings['schedule']['hooks']['sync'] );
        if( !$timeNext ) {
            return false;
        }

        wp_unschedule_event( $timeNext, $settings['schedule']['hooks']['sync'] );
    }
}
