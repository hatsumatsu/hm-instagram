<?php

class HMInstagram_Admin {
    public function __construct() {
        $this->init();
    }


    /**
     * Init
     */
    public function init() {
        // add admin menu item
        add_action( 'admin_menu', array( $this, 'addMenuItem' ) );

        // register plugin's admin CSS
        add_action( 'admin_enqueue_scripts', array( $this, 'adminCSS' ) );

        // modify post columns
        add_filter( 'manage_edit-instagram_columns', array( $this, 'modifyPostColumns' ) );

        // populate post columns
        add_action( 'manage_instagram_posts_custom_column', array( $this, 'populatePostColumns' ), 10, 2 );
    }



    /**
     * Register plugin's admin CSS
     */
    public function adminCSS() {
        wp_register_style( 'hm-instagram--admin', WP_PLUGIN_URL . '/hm-instagram/css/hm-instagram--admin.css' );
        wp_enqueue_style( 'hm-instagram--admin' );
    }


    /**
     * Add admin area page
     */
	public function addMenuItem() {
        add_menu_page(
            __( 'Instagram', 'hm-instagram' ), // page <title>
            __( 'Instagram', 'hm-instagram' ), // menu title
            'connect_instagram', // capability
            __( 'instagram', 'hm-instagram' ), // menu slug
            array( $this, 'displayOptionPage' ), // render callback
            'dashicons-camera', // menu icon
            60 // menu position
        );
    }


    /**
     * Render admin area page
     */
    public function displayOptionPage() {
        $settings = HMInstagram_Core::getSettings();
        $apiSettings = get_option( 'instagram--api' );

        if( !current_user_can( 'manage_options' ) ) {

        }
?>
    <div class="wrap">
        <h1>
        	<?php echo __( 'Instagram', 'hm-instagram' ); ?>
        </h1>


        <div class="metabox-holder">

<?php
        if( HMInstagram_Auth::getAccessToken() ) {
?>
            <div class="postbox postbox--instagram-overview connected">
                <div class="inside">
                    <p>
                        <?php echo sprintf(
                            __( 'Your website is connect with Instagram account <a href="%s">%s</a>.', 'hm-instagram' ),
                            esc_url(
                                sprintf(
                                    'https://instagram.com/%s/',
                                    HMInstagram_Auth::getUsername()
                                )
                            ),
                            HMInstagram_Auth::getUsername()
                            ); ?>
                    </p>

                    <p>
                        <a href="<?php echo esc_url( get_admin_url( 0, '?page=instagram&action=disconnect' ) ); ?>" class="button button-primary">
                            <?php echo __( 'Disconnect from Instagram', 'hm-instagram' ); ?>
                        </a>
                    </p>

<?php
            if( $url = get_option( $settings['meta']['profile_image'] ) ) {
?>
                    <a class="instagram-overview-image">
                        <img src="<?php echo esc_url( $url ); ?>" class="instagram-overview-image-image">
                    </a>
<?php
            }
?>
                </div>
            </div>
<?php
        } else {
?>
            <div class="postbox postbox--instagram-overview disconnected">
                <div class="inside">
                    <p>
                        <?php echo sprintf( __( 'Your website is not connected with Instagram.', 'hm-instagram' ) ); ?>
                    </p>
<?php
            if( $apiSettings['client_id'] && $apiSettings['client_secret'] ) {
?>
                    <p>
                        <a href="<?php echo esc_url( $settings['API']['url']['auth'] ); ?>" class="button button-primary">
                            <?php echo __( 'Connect with Instagram', 'hm-instagram' ); ?>
                        </a>
                    </p>
<?php
            }
?>
                </div>
            </div>
<?php
        }
?>

<?php
        if( HMInstagram_Auth::getAccessToken() ) {
?>
            <div class="postbox postbox--instagram-status">
                <h3 class="hndle">
                    <?php echo __( 'Status', 'hm-instagram' ); ?>
                </h3>
                <div class="inside">

<?php
        // synced instagram posts
        $posts = HMInstagram_Posts::getPosts(
            array(
                'posts_per_page' => 1
            )
        );
        // last sync time
        $time = HMInstagram_Stats::getLastSyncTime();

        // number of posts synced
        $total = HMInstagram_Stats::getImagesTotal();

        if( $posts && $time ) {
            $post = $posts[0];
            $image_id = get_post_thumbnail_id( $post->ID );

            if( $image_id ) {
                $image_src = wp_get_attachment_image_src( $image_id );
                $url = get_post_meta( $post->ID, 'instagram--post_url', true );
?>
                    <a href="<?php echo esc_url( $url ); ?>" target="_blank" class="latest-image">
                        <img src="<?php echo esc_url( $image_src[0] ); ?>" class="latest-image-image">
                    </a>

<?php
            }
        }
?>

                    <section class="stats">

<?php
        if( $time ) {
?>
                        <article class="stat">
                            <span class="stat-label">
                                <?php echo __( 'Last sync' ); ?>
                            </span>

                            <span class="stat-value" title="<?php echo esc_attr( wptexturize( date( get_option( 'date_format' ), $time ) . ' ' . date( get_option( 'time_format' ), $time ) ) ); ?>">
                                <?php echo sprintf( __( '%s ago', 'hm-instagram' ),  wptexturize( human_time_diff( $time ) ) ); ?>
                            </span>
                        </article>
<?php
        } else {
?>
            <span class="label-background label-background--waiting">
                <?php echo __( 'Syncing...', 'hm-instagram' ); ?>
            </span>
<?php
        }
?>


<?php
        if( $total && $time ) {
?>
                        <article class="stat">
                            <span class="stat-label">
                                <?php echo __( 'Synced images' ); ?>
                            </span>

                            <span class="stat-number">
                                <?php echo wptexturize( $total ); ?>
                            </span>
                        </article>
<?php
        }
?>

                    </section>

                    <p class="clear"></p>

                </div>
            </div>

<?php
        }
?>

        </div>

    </div>


<?php
    }


    /**
     * Modify columns in post list
     * + Add thumbnail image
     * + Remove post type, author
     * @param   array   $columns    columns of the edit events list
     * @return  array   $columns
     */
    public function modifyPostColumns( $columns ) {
        $columns['instagram--thumbnail']  = __( 'Thumbnail', 'port-f' );

        unset(
            $columns['author'],
            $columns['post_type']
        );

        return $columns;
    }


    /**
     * Show values in custom column in post list
     *
     * @param   int     $column_name    name of the columns to render values in.
     * @param   int     $post_id        post ID
     */
    public function populatePostColumns( $column_name, $post_id ) {
        $settings = HMInstagram_Core::getSettings();

        // thumbnail image
        if( $column_name === 'instagram--thumbnail' ) {
            if( $image_id = get_post_meta( $post_id, '_thumbnail_id', true ) ) {
                $url = get_post_meta( $post_id, 'instagram--post_url', true );
                if( $url ) {
                    echo '<a href="' . esc_url( $url ) . '" target="_blank">';
                }

                echo wp_get_attachment_image( $image_id, 'thumbnail' );

                if( $url ) {
                    echo '</a>';
                }
            }
        }
    }
}
