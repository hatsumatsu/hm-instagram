<?php

class HMInstagram_Stats {
    public function __construct() {
        $this->init();
    }


    /**
     * Init
     */
    public function init() {

    }


    /**
     * Get time of last synchronisation
     *
     * @return integer timestamp
     */
    public static function getLastSyncTime() {
        $settings = HMInstagram_Core::getSettings();

        return get_option( $settings['meta']['updated'] );
    }


    /**
     * Get total number of imported Instagram images
     * @param  integer $user_id user ID
     * @return integer          number of images
     */
    public static function getImagesTotal() {
        $settings = HMInstagram_Core::getSettings();

        $posts = get_posts(
            array(
                'post_type' => 'instagram',
                'posts_per_page' => -1,
                'fields' => 'ids'
            )
        );


        if( !$posts ) {
            return 0;
        }

        return count( $posts );
    }
}
