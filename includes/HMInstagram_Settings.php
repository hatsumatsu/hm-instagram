<?php
/**
 * https://github.com/tareq1988/wordpress-settings-api-class/
 */
class HMInstagram_Settings {
    private $settingsAPI;


    public function __construct() {
        $this->init();
    }


    /**
     * Init
     */
    public function init() {
        // setup settings API
        add_action( 'admin_init', array( $this, 'setup' ) );

        // register hook when saving settings
        add_action( 'admin_init', array( $this, 'save' ) );

        // register options page
        add_action( 'admin_menu', array( $this, 'addOptionPage' ) );
    }


    /**
     * Setup settings API
     */
    public function setup() {
        $this->settingsAPI = new WeDevs_settings_api;

        // initialize the settings API
        $this->settingsAPI->set_sections( $this->getSettingsSections() );
        $this->settingsAPI->set_fields( $this->getSettingsFields() );
        $this->settingsAPI->admin_init();
    }


    /**
     * Register hook when saving settings
     */
    public function save() {
        if( $_GET['page'] === 'hm-instagram' && $_GET['settings-updated'] ) {
            if( !current_user_can( 'manage_options' ) ) {
                return false;
            }

            add_filter( 'cron_schedules', 'HMInstagram_Schedule::registerInterval' );
            HMInstagram_Schedule::unSchedule();
            HMInstagram_Schedule::schedule();
        }
    }


    /**
     * Register options page
     */
	public function addOptionPage() {
        add_options_page(
            __( 'HM Instagram', 'hm-instagram' ),
            __( 'HM Instagram', 'hm-instagram' ),
            'manage_options',
            'hm-instagram',
            array( $this, 'renderOptionPage' )
        );
    }


    /**
     * Render the options page
     */
    public function renderOptionPage() {
?>
<div class="wrap">

    <h1>
        <?php echo __( 'Settings › Instagram', 'hm-instagram' ); ?>
    </h1>

<?php
        $this->settingsAPI->show_navigation();
        $this->settingsAPI->show_forms();
?>

    <div class="metabox-holder">

        <div class="postbox postbox--instagram-status">
            <h3 class="hndle">
                <?php echo __( 'Status', 'hm-instagram' ); ?>
            </h3>
            <div class="inside">


<?php
        $posts = HMInstagram_Posts::getPosts(
            array(
                'posts_per_page' => 1
            )
        );

        if( $posts ) {
            $post = $posts[0];
            $image_id = get_post_thumbnail_id( $post->ID );
            if( $image_id ) {
                $image_src = wp_get_attachment_image_src( $image_id );
                $url = get_post_meta( $post->ID, 'instagram--post_url', true );
?>
                <a href="<?php echo esc_url( $url ); ?>" target="_blank" class="latest-image">
                    <img src="<?php echo esc_url( $image_src[0] ); ?>" class="latest-image-image">
                </a>
<?php
            }
        }
?>

                <section class="stats">

<?php
    if( $time = HMInstagram_Stats::getLastSyncTime() ) {
?>
                    <article class="stat">
                        <span class="stat-label">
                            <?php echo __( 'Last sync' ); ?>
                        </span>

                        <span class="stat-value" title="<?php echo esc_attr( wptexturize( date( get_option( 'date_format' ), $time ) . ' ' . date( get_option( 'time_format' ), $time ) ) ); ?>">
                            <?php echo sprintf( __( '%s ago', 'hm-instagram' ),  wptexturize( human_time_diff( $time ) ) ); ?>
                        </span>
                    </article>
<?php
    }
?>


<?php
    if( $total = HMInstagram_Stats::getImagesTotal() ) {
?>
                    <article class="stat">
                        <span class="stat-label">
                            <?php echo __( 'Synced images' ); ?>
                        </span>

                        <span class="stat-number">
                            <?php echo wptexturize( $total ); ?>
                        </span>
                    </article>
<?php
    }
?>
                </section>

                <p class="clear"></p>
            </div>
        </div>
    </div>
</div>

<?php
    }


    /**
     * Get all the settings sections
     *
     * @return array settings fields
     */
    public function getSettingsSections() {
        $sections = array(
            array(
                'id'    => 'instagram--sync',
                'title' => __( 'Sync settings', 'hm-instagram' )
            ),
            array(
                'id'    => 'instagram--api',
                'title' => __( 'API settings', 'hm-instagram' )
            ),
            array(
                'id'    => 'instagram--debug',
                'title' => __( 'Debug settings', 'hm-instagram' )
            )
        );

        return $sections;
    }


    /**
     * Get all the settings fields
     *
     * @return array settings fields
     */
    public function getSettingsFields() {
        $settings_fields = array(
            'instagram--sync' => array(
                array(
                    'name'              => 'interval',
                    'label'             => __( 'Sync interval', 'hm-instagram' ),
                    'desc'              => __( 'Time in minutes between each sync.', 'hm-instagram' ),
                    'min'               => 1,
                    'max'               => 1440,
                    'step'              => '1',
                    'type'              => 'number',
                    'default'           => 30,
                    'sanitize_callback' => 'intval'
                ),
                array(
                    'name'              => 'max_images',
                    'label'             => __( 'Max images', 'hm-instagram' ),
                    'desc'              => __( 'Max number of images kept in Wordpress.', 'hm-instagram' ),
                    'min'               => 1,
                    'max'               => 9999,
                    'step'              => '1',
                    'type'              => 'number',
                    'default'           => 20,
                    'sanitize_callback' => 'intval'
                ),
                array(
                    'name'              => 'delete_unused_images',
                    'label'             => __( 'Delete old images', 'hm-instagram' ),
                    'type'              => 'checkbox'
                ),
                array(
                    'name'              => 'taxonomy_tags',
                    'label'             => __( 'Taxonomy for tags', 'hm-instagram' ),
                    'type'              => 'text',
                    'default'           => ''
                )
            ),
            'instagram--api' => array(
                array(
                    'name'              => 'client_id',
                    'label'             => __( 'Client ID', 'hm-instagram' ),
                    'type'              => 'text',
                    'default'           => ''
                ),
                array(
                    'name'              => 'client_secret',
                    'label'             => __( 'Client Secret', 'hm-instagram' ),
                    'type'              => 'text',
                    'default'           => ''
                )
            ),
            'instagram--debug' => array(
                array(
                    'name'              => 'logging',
                    'label'             => __( 'Logging', 'hm-instagram' ),
                    'type'              => 'checkbox',
                )
            )
        );

        return $settings_fields;
    }


    /**
     * Get all the pages
     *
     * @return array page names with key value pairs
     */
    function getPages() {
        $pages = get_pages();
        $pages_options = array();
        if( $pages ) {
            foreach( $pages as $page ) {
                $pages_options[$page->ID] = $page->post_title;
            }
        }

        return $pages_options;
    }
}
