<?php

class HMInstagram_Auth {
	public function __construct() {
        $this->init();
	}


    /**
     * Init
     */
    public function init() {
        // handle auth redirect
        add_action( 'admin_init', array( $this, 'handleAuthRedirect' ) );

        // handle disconnect
        add_action( 'admin_init', array( $this, 'handleDisconnect' ) );
    }


    /**
     * Handle auth redirect from Instagram API
     * https://www.instagram.com/developer/authentication/
     */
    public function handleAuthRedirect() {
        if( $_REQUEST['page'] != 'instagram' ) {
            return false;
        }

        $code = $_REQUEST['code'];

        if( !$code ) {
            return false;
        }

        $access_token = $this->fetchAccessToken( $code );

        if( !$access_token ) {
            return false;
        }

        self::connect( $access_token );
    }


    /**
     * Handle disconnect from Instagram API
     */
    public function handleDisconnect() {
        if( $_REQUEST['page'] != 'instagram' || $_REQUEST['action'] != 'disconnect' ) {
            return false;
        }

        self::disconnect( get_current_user_id() );
    }


    /**
     * Fetch access token from Instagram API based on code submitted by auth redirect
     * @param  string $code auth code
     * @return string       Access Token
     */
    public function fetchAccessToken( $code ) {
        $settings = HMInstagram_Core::getSettings();
        $apiSettings = ( get_option( 'instagram--api' ) ) ? get_option( 'instagram--api' ) : $settings['API'];

        if( !$code ) {
            return false;
        }

        $url = $settings['API']['url']['access_token'];

        $response = wp_remote_post( $url, array(
            'body' => array(
                'client_id'     => $apiSettings['client_id'],
                'client_secret' => $apiSettings['client_secret'],
                'grant_type'    => 'authorization_code',
                'redirect_uri'  => $settings['API']['redirect_url'],
                'code'          => $code
            ),
        ) );

        if( $response['response']['code'] != 200 ) {
            return false;
        }

        $response = json_decode( $response['body'] );

        if( !$response || !$response->access_token ) {
            return false;
        }

        // save access token as WP option
        // update_user_meta( get_current_user_id(), $settings['meta']['access_token'], $response->access_token );
        update_option( $settings['meta']['access_token'], $response->access_token );

        return $response->access_token;
    }


    /**
     * Get user data from Instagram API
     */
    public function fetchUserInfo() {
        $settings = HMInstagram_Core::getSettings();

        $access_token = self::getAccessToken();

        if( !$access_token ) {
            return false;
        }

        HMInstagram_Log::log( __( 'Sync Instagram user info.', 'hm-instagram' ) );

        $url = str_replace( '{{access_token}}', $access_token, $settings['API']['url']['user'] );
        $response = wp_remote_get( $url );

        if( $response['response']['code'] != 200 ) {
            return false;
        }

        $response = json_decode( $response['body'] );

        if( !$response || !$response->data->username ) {
            return false;
        }

        // save Instagram user name as option
        update_option( $settings['meta']['username'], $response->data->username );

        // save Instagram profile image as option
        update_option( $settings['meta']['profile_image'], $response->data->profile_picture );
    }


    /**
     * Connect to Instagram API and initiate user
     * @param  string $access_token API access token
     */
    public function connect( $access_token ) {
        $settings = HMInstagram_Core::getSettings();

        // fetch user info
        self::fetchUserInfo();

        // init the user's sync timestamp
        update_option( $settings['meta']['updated'], 0 );
    }


    /**
     * Disconnect user from Instagram API
     * @param  int $user_id user ID
     */
    public function disconnect() {
        $settings = HMInstagram_Core::getSettings();

        // delete the user's posts
        HMInstagram_Posts::deletePosts();

        // delete access token
        delete_option( $settings['meta']['access_token'] );

        // delete last sync timestamp
        delete_option( $settings['meta']['updated'] );

        // delete Instagram user name
        delete_option( $settings['meta']['username'] );

        // delete newest image time
        delete_option( $settings['meta']['newest_image_time'] );
    }


    /**
     * Get Access token
     * @return string $access_token access token
     */
    public static function getAccessToken() {
        $settings = HMInstagram_Core::getSettings();

        return get_option( $settings['meta']['access_token'] );
    }


    /**
     * Get instagram user name for current user
     * @param  int $user_id user ID
     * @return string       user name
     */
    public static function getUsername() {
        $settings = HMInstagram_Core::getSettings();

        return get_option( $settings['meta']['username'] );
    }
}
