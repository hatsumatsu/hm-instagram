<?php

class HMInstagram_Sync {
    public function __construct() {
        $this->init();
    }


    /**
     * Init
     */
    public function init() {
        $settings = HMInstagram_Core::getSettings();

        add_action( $settings['schedule']['hooks']['sync'], array( $this, 'sync' ) );
    }


    /**
     * General sync routine
     */
    public static function sync() {
        $settings = HMInstagram_Core::getSettings();
        $syncSettings = ( get_option( 'instagram--sync' ) ) ? get_option( 'instagram--sync' ) : $settings['sync'];

        if( !get_option( 'hm-instagram--bot_user_id' ) ) {
            return false;
        }

        HMInstagram_Log::log( __( 'Start syncing with Instagram', 'hm-instagram' ) );

        // fetch images
        self::fetchImages();

        // delete unused images
        if( $syncSettings ) {
            if( $syncSettings['delete_unused_images'] === 'on' ) {
                self::deleteUnusedImages();
            }
        }

        // update user info
        HMInstagram_Auth::fetchUserInfo();

        // save time the users was synced
        update_option( $settings['meta']['updated'], time() );
    }


    /**
     * fetch latest images by user id from API
     */
    public static function fetchImages() {
        $settings = HMInstagram_Core::getSettings();
        $syncSettings = ( get_option( 'instagram--sync' ) ) ? get_option( 'instagram--sync' ) : $settings['sync'];

        $access_token = HMInstagram_Auth::getAccessToken();

        if( !$access_token ) {
            HMInstagram_Log::log( __( 'Could not find access token', 'hm-instagram' ) );

            return false;
        }

        HMInstagram_Log::log( sprintf( __( 'Access token is %s.', 'hm-instagram' ), $access_token ) );

        $last_image_id = get_option( $settings['meta']['newest_image'] );
        $last_image_time = get_option( $settings['meta']['newest_image_time'] );

        if( !$last_image_time ) {
            $last_image_time = 0;
        }

        $url = str_replace( '{{access_token}}', $access_token, $settings['API']['url']['media'] );
        $url = $url . '&count=' . $syncSettings['max_images'];

        // TODO: use the min_id API parameter once it works again...
        // if( $last_image_id ) {
        //     $url = $url . '&min_id=' . $last_image_id;
        // }

        $response = wp_remote_get( $url );

        if( is_wp_error( $response ) || $response['response']['code'] != 200 ) {
            HMInstagram_Log::log( sprintf( __( 'There was an error (1) while syncing with Instagram.', 'hm-instagram' ) ) );
            HMInstagram_Log::log( $url );
            if( $response['code'] ) {
                HMInstagram_Log::log( 'Respoonse code: ' . $response['code'] );
            }

            return false;
        }

        $response = json_decode( $response['body'] );

        if( !$response || !$response->data || !count( $response->data ) ) {
            HMInstagram_Log::log( sprintf( __( 'There was an error (2) while syncing with Instagram.', 'hm-instagram' ) ) );

            return false;
        }

        $last_image_added_time = $last_image_time;

        foreach( $response->data as $image ) {
            // skip images older than the time of the last synced image
            if( intval( $image->created_time ) <= intval( $last_image_time ) ) {
                HMInstagram_Log::log( sprintf( __( 'Image found is older than our latest image. %s / %s', 'hm-instagram' ), $image->created_time, $last_image_time ) );

                continue;
            }

            // skip if image has no image data
            if( !$image->images || !$image->images->standard_resolution ) {
                HMInstagram_Log::log( sprintf( __( 'Image found has no standard resolution entry...', 'hm-instagram' ) ) );

                continue;
            }

            // fetch image file
            $url = $image->images->standard_resolution->url;

            $attachment_id = self::fetchImage( $url );

            if( !$attachment_id ) {
                HMInstagram_Log::log( sprintf( __( 'There was an error uploading image %s.', 'hm-instagram' ), $url ) );

                continue;
            }

            // create post
            $content = ( $image->caption->text ) ? $image->caption->text : '';

            $meta = array(
                $settings['meta']['post_url'] => $image->link,
                $settings['meta']['tags'] => $image->tags
            );

            if( $image->location ) {
                $meta[ $settings['meta']['location'] ] = $image->location;
            }

            if( $image->type === 'video' ) {
                $meta[ $settings['meta']['is_video'] ] = 1;
            }

            // insert post
            $post_id = wp_insert_post(
                array(
                    'post_type' => 'instagram',
                    'post_author' => get_option( 'hm-instagram--bot_user_id' ),
                    'post_date' => date( 'Y-m-d H:i:s', intval( $image->created_time ) ),
                    'post_title' => $image->id,
                    'post_content' => $content,
                    'post_status' => 'publish',
                    'meta_input' => $meta
                )
            );

            // assign tags
            if( $image->tags ) {
                if( $syncSettings['taxonomy_tags'] ) {
                    if( taxonomy_exists( $syncSettings['taxonomy_tags'] ) ) {
                        wp_set_object_terms( $post_id, $image->tags, $syncSettings['taxonomy_tags'] );
                    }
                }
            }

            // set fetched image as featured image
            update_post_meta( $post_id, '_thumbnail_id', $attachment_id );

            // set attachment post_parent to post id
            wp_update_post( array(
                'ID' => $attachment_id,
                'post_parent' => $post_id
            ) );

            if( intval( $image->created_time ) > $last_image_added_time ) {
                $last_image_added_time = intval( $image->created_time );
            }
        }

        HMInstagram_Log::log( sprintf( __( 'Last image added: %s / %s.', 'hm-instagram' ), $last_image_added_time, $last_image_time ) );

        // save newest image time
        update_option( $settings['meta']['newest_image_time'], $last_image_added_time );
    }


    /**
     * Fetch remote image by url and create an attachment
     * @param  string $url            image url
     * @return int|false              attachment ID or false
     */
    public static function fetchImage( $url ) {
        $settings = HMInstagram_Core::getSettings();

        if( !$url ) {
            return false;
        }

        // sanitize file name, delete query string
        if( strpos( $url, '?' ) !== false ) {
            $fileNameUrl = explode( '?', $url )[0];
        } else {
            $fileNameUrl = $url;
        }

        $filename = basename( $fileNameUrl );

        // upload file
        $upload = wp_upload_bits( $filename, null, file_get_contents( $url ) );

        if( !$upload['error'] ) {
            // create attachment post
            $filetype = wp_check_filetype( $filename, null );
            $attachment = array(
                'post_mime_type' => $filetype['type'],
                'post_title' => preg_replace( '/\.[^.]+$/', '', $filename ),
                'post_content' => '',
                'post_status' => 'inherit',
                'post_author' => get_option( 'hm-instagram--bot_user_id' )
            );
            $attachment_id = wp_insert_attachment( $attachment, $upload['file'] );

            // generate attachment metadata
            if( !is_wp_error( $attachment_id ) ) {
                require_once( ABSPATH . 'wp-admin/includes/image.php' );
                $attachment_data = wp_generate_attachment_metadata( $attachment_id, $upload['file'] );
                wp_update_attachment_metadata( $attachment_id, $attachment_data );
            } else {
                return false;
            }

            // check if image is broken
            $source = wp_get_attachment_image_src( $attachment_id, 'thumbnail' );
            if( !$source[1] ) {
                HMInstagram_Log::log( sprintf( __( 'Image %s seems broken (%s).', 'hm-instagram' ), $url, $attachment_id ) );

                return false;
            }

            HMInstagram_Log::log( sprintf( __( 'Fetched image %s (%s).', 'hm-instagram' ), $attachment_id, $url ) );

            return $attachment_id;
        } else {
            return false;
        }
    }


    /**
     * Delete unsused posts by user
     * @param  integer $user_id user ID
     */
    public static function deleteUnusedImages() {
        $settings = HMInstagram_Core::getSettings();
        $syncSettings = ( get_option( 'instagram--sync' ) ) ? get_option( 'instagram--sync' ) : $settings['sync'];

        $posts = HMInstagram_Posts::getPosts(
            array(
                'posts_per_page' => 9999,
                'offset'    => $syncSettings['max_images']
            )
        );

        if( !$posts ) {
            return false;
        }

        foreach( $posts as $post ) {
            HMInstagram_Posts::deletePost( $post->ID );
        }

        HMInstagram_Log::log( sprintf( __( 'Deleted %s unused Instagram posts.', 'hm-instagram' ), count( $posts ) ) );
    }
}
